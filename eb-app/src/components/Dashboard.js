import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useAuth } from "../context/AuthContext";
import { Card, Button, Alert } from "react-bootstrap";

export default function Dashboard() {
    const { currentUser, logout } = useAuth();
    const [error, setError] = useState('');
    const history = useHistory();

    async function handleLogout() {
        setError('');

        try {
            await logout();
            history.push('./login');
        } catch {
            setError('Failed to log out!');
        }
    }

    return (
        <>
            <Card>
                <Card.Body>
                    <h2 className="text-center mb-4">Profile</h2>
                    {error && <Alert variant="danger">{ error }</Alert>}
                    <p>Email: { currentUser.email }</p>
                </Card.Body>
            </Card>
            <div className="w-100 text-center mt-2">
                <Button variant="link" onClick={handleLogout}>Log Out</Button>
            </div>
        </>
    )
}
