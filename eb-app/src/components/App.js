import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AuthProvider } from "../context/AuthContext";
import PrivateRoute from "../routes/PrivateRoute";
import Authentication from "./Authentication";
import Dashboard from "./Dashboard";
import { Container } from "react-bootstrap";

function App() {
    return (
        <Container
            className="d-flex align-items-center justify-content-center"
            style={{ minHeight: "100vh" }}
        >
            <div className="w-100" style={{ maxWidth: "400px" }}>
                <Router>
                    <AuthProvider>
                        <Switch>
                            <PrivateRoute exact path="/" component={ Dashboard }/>
                            <Route path="/register" component={ Authentication } />
                            <Route path="/login" component={ Authentication } />
                        </Switch>
                    </AuthProvider>
                </Router>
            </div>
        </Container>
    );
}

export default App;
