import React, { useRef, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../context/AuthContext";
import { Form, Button, Card, Alert } from "react-bootstrap";

export default function Authentication() {
    const emailRef = useRef();
    const passwordRef = useRef();
    const passwordConfirmRef = useRef();
    const { register, login } = useAuth();
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const [loginView, setLoginView] = useState(false);
    const history = useHistory();

    async function handleRegisterSubmit(e) {
        e.preventDefault();

        if (passwordRef.current.value !== passwordConfirmRef.current.value) {
            return setError('Passwords do not match!');
        }

        try {
            setError('');
            setLoading(true);
            await register(emailRef.current.value, passwordRef.current.value);
            history.push('/');
        } catch {
            setError('Failed to create an account!');
        }

        setLoading(false);
    }

    async function handleLoginSubmit(e) {
        e.preventDefault();

        try {
            setError('');
            setLoading(true);
            await login(emailRef.current.value, passwordRef.current.value);
            history.push('/');
        } catch {
            setError('Failed to log in!');
        }

        setLoading(false);
    }

    function toggleAuth(tab) {
        if (tab.currentTarget.id === 'login' ) {
            setLoginView(true);
        } else {
            setLoginView(false);
        }
    }

    return (
        <>
            <Card>
                <Card.Body>
                    <div className="d-flex justify-content-around">
                        <Link to="/register" className="w-50 text-center mb-4">
                            <Button id="register" onClick={ toggleAuth }>Register</Button>
                        </Link>
                        <Link to="/login" className="w-50 text-center mb-4">
                            <Button id="login" onClick={ toggleAuth }>Login</Button>
                        </Link>
                    </div>
                    { error && <Alert variant="danger">{ error }</Alert>}
                    { loginView || (window.location.reload && history.location.pathname === "/login") ?
                        <Form onSubmit={handleLoginSubmit}>
                            <Form.Group id="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" ref={emailRef} required/>
                            </Form.Group>
                            <Form.Group id="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" ref={passwordRef} required/>
                            </Form.Group>
                            <Button type="submit" disabled={loading} className="w-100 mt-4">Log In</Button>
                        </Form>
                    :
                        <Form onSubmit={ handleRegisterSubmit }>
                            <Form.Group id="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" ref={ emailRef } required />
                            </Form.Group>
                            <Form.Group id="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" ref={ passwordRef } required />
                            </Form.Group>
                            <Form.Group id="password-confirm">
                                <Form.Label>Password Confirmation</Form.Label>
                                <Form.Control type="password" ref={ passwordConfirmRef } required />
                            </Form.Group>
                            <Button type="submit" disabled={ loading } className="w-100 mt-4">Register</Button>
                        </Form>
                    }
                </Card.Body>
            </Card>
        </>
    )
}